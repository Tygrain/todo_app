import React, { useState } from 'react';

function InputItem(props) {
  const [input, setInput] = useState(props.editTask ? props.value : '');

  let newTask = () => {
    if (input) {
      setInput('');
      props.addTask(input);
    }
  }

  let editTask = () => {
    if (input) {
      props.editTask(input);
    }
  }


  let handleChange = (event) => {
    setInput(event.target.value);
  }

  if (props.editTask)
    return (
      <input className="edit-item" onChange={handleChange} value={input} onKeyPress={(event) => event.key === 'Enter' && editTask()} />
    );

  return (
    <input className="input-item" onChange={handleChange} value={input} onKeyPress={(event) => event.key === 'Enter' && newTask()} placeholder="Add new task..." />
  );
}

export default InputItem;