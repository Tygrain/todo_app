import React, { useState } from 'react';
import InputItem from './InputItem';

function TaskItem({ item, ...props }) {
  const [editMode, setEditMode] = useState(false);

  const TaskCheckBox = () => {
    const style = {
      visibility: (props.editBar)? 'hidden': ''
    };
    if (item.active) {
      return (
        <input type="checkbox" style={style} onClick={props.doneTask} />
      );
    }
    return (
      <input type="checkbox" style={style} checked disabled />
    );
  };

  let editTask = (task) => {
    props.editTask(item.id, task);
    setEditMode(false);
  }


  if (editMode)
  return (<InputItem editTask={editTask} value={item.text}></InputItem>);

  const className = 'item' + (!item.active ? '-done' : '');
  return (
    <div className={className}>
      <TaskCheckBox></TaskCheckBox>
      <div className="item-text">{item.text}</div>
      <div className="edit-bar">
      {(props.editBar && item.active)? (<a href="#edit" onClick={() => setEditMode(true)}><img className="task-edit-button" src="./edit.svg" /></a>) : ''}
      {(props.editBar)? (<a href="#remove" onClick={() => props.removeTask(item.id)}><img className="task-clear-button" src="./clear.svg" /></a>) : ''}
      </div>
    </div>
  );
}

export default TaskItem;