import React, { useState } from 'react';
import {useLocalStorage} from '../LocalStorage';
import TaskItem from './TaskItem';
import InputItem from './InputItem';
import '../App.css';

function App (){
  const [localStorageItems, setLocalStorageItems] = useLocalStorage();
  const [editMode, setEditMode] = useState(false);
  const [items, setItems] = useState(localStorageItems ? localStorageItems : []);

  let doneTask = (id) => {
    const index = items.map(task => task.id).indexOf(id);
    setItems((tasks) => {
      tasks = items.slice();
      tasks[index].active = false;
      setLocalStorageItems(tasks);
      return tasks;
    });
  }

  let removeTask = (id) => {
    setItems((tasks) => {
      tasks = items.filter((item) => item.id !== id)
      for(let i in tasks)
        tasks[i].id = i;
      setLocalStorageItems(tasks);
      return tasks;
    });
  }

  let editTask = (id, task) => {
    const index = items.map(task => task.id).indexOf(id);
    setItems((tasks) => {
      tasks = items.slice()
      tasks[index].text = task;
      setLocalStorageItems(tasks);
      return tasks;
    });
  }

  let addTask = (task) => {
    setItems((tasks) => {
      tasks = [...items, {
        id: items.length,
        text: task,
        active: true
      }];
      setLocalStorageItems(tasks);
      return tasks;
    });
  }

  return (
    <div className="App">
      <div className="title-bar">
        ToDo List
        <a href="#edit-list" onClick={() => setEditMode(editMode? false : true)}><img className="task-list-button" src="./list.svg" /></a>
      </div>
      {items.map(task => (
        <TaskItem item={task} key={task.id} doneTask={() => doneTask(task.id)} editTask={editTask} removeTask={removeTask} editBar={editMode}></TaskItem>
        ))}
      <InputItem addTask={addTask}></InputItem>
      <div className="blockpage"></div>
    </div>
  );
}

export default App;
